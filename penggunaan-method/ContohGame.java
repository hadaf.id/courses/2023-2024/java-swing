class PengelolaKalimat {
  public static void gabungkanKalimat(String a, String b) {
    System.out.println(a + b);
  }

  public void gabungkanKalimat2(String a, String b) {
    System.out.println(a + b);
  }
}

class Orang {
  int uang = 0;

  int getUang() {
    return this.uang;
  }

  void setUang(int jumlahUang) {
    // aturan2

    this.uang = jumlahUang;
  }

  void printUang() {
    System.out.println(this.getUang());
  }
}

class Hero {
  String name;
  int hp = 1000;
  int baseAttack = 50;

  // Method nilai kembali void, parameter object dari class Hero
  void attack(Hero otherHero) {
    otherHero.hp = otherHero.hp - this.baseAttack;
    System.out.println(this.name + " menyerang " + otherHero.name + " Sehingga hp " + otherHero.name + " berkurang menjadi " + otherHero.hp);
  }
}

public class ContohGame {

  // Method nilai kembali void, artinya tidak mengembalikan nilai apapun
  // Tanpa parameter
  void luncurkanRoket () {
    System.out.println("Roket diluncurkan");
  }

  // Method nilai kembali void,
  // Dengan parameter
  void luncurkanRoketSejumlah (int jumlah) {
    System.out.println("Roket sejumlah " + jumlah + " diluncurkan");
  } 

  // Method nilai kembali void,
  // Dengan parameter berupa Object
  void pinjamUang (Orang orang, int jumlahPeminjaman) {

    orang.uang = orang.uang - jumlahPeminjaman;
    System.out.println("Pinjang uang ke orang sejumlah " + jumlahPeminjaman);
    System.out.println("Sehingga, si orang uangnya sekarang " + orang.uang);

  } 

  public static void main (String[] argumen) {
    System.out.println("Halo Method");

    // Pendefinisian object dari class ContohGame ke dalam variable game
    ContohGame g = new ContohGame();

    g.luncurkanRoket();
    g.luncurkanRoketSejumlah(5);

    Orang eka = new Orang();
    eka.uang = 4000000;

    g.pinjamUang(eka, 100000);

    Hero hero1 = new Hero();
    hero1.name = "Rio";
    hero1.hp = 1000;
    hero1.baseAttack = 65;

    Hero hero2 = new Hero();
    hero2.name = "Raka";
    hero2.hp = 2000;
    hero2.baseAttack = 30;

    hero1.attack(hero2);
    hero1.attack(hero2);
    hero1.attack(hero2);
    hero1.attack(hero2);
    hero1.attack(hero2);

    hero2.attack(hero1);
    hero2.attack(hero1);
    hero2.attack(hero1);

    System.out.println("Uang eka sekarang " + eka.uang);

    // Memanggil method static dari class PengelolaKalimat
    PengelolaKalimat.gabungkanKalimat("Hari Sabtu", " beli roti");

    // Membuat object dari class PengelolaKalimat
    PengelolaKalimat pK = new PengelolaKalimat();

    // Memanggil method dari object pK yang dibuat dari class PengelolaKalimat
    pK.gabungkanKalimat2("Hari Minggu", " sewa kuda");


  }
}
