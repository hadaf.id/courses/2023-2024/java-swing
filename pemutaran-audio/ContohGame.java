import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

/**
 * Nama class: ActionListener
 * Package: java.awt.event
 * Tanggung jawab: memantau event dan mendelegasikan aksi ketika event tersebut terjadi
 */
import java.awt.event.ActionListener;

/**
 * Nama class: ActionEvent
 * Package: java.awt.event
 * Tanggung jawab: merepresentasikan event aksi dari komponen
 */
import java.awt.event.ActionEvent;

/**
 * Nama class: File 
 * Package: java.io
 * Tanggung jawab: mengelola dan merepresentasikan file dan alamat direktorinya
 */
import java.io.File; 

/**
 * Nama class: AudioSystem 
 * Package: javax.sound.sampled
 * Tanggung jawab: Mengelola berbagai kebutuhan audio 
 */
import javax.sound.sampled.AudioSystem; 

/**
 * Nama class: AudioInputStream 
 * Package: javax.sound.sampled
 * Tanggung jawab: Merepresentasikan audio 
 */
import javax.sound.sampled.AudioInputStream; 

/**
 * Nama class: Clip 
 * Package: javax.sound.sampled
 * Tanggung jawab: Memainkan audio 
 */
import javax.sound.sampled.Clip; 

import javax.swing.BoxLayout;

class ContohGame {
  public static void main(String[] args) {

    JFrame wadahUtama = new JFrame();

    wadahUtama.setSize(800, 600); 
    wadahUtama.setTitle("Jelajah Bangunan Kuno");
    wadahUtama.setLayout(new BoxLayout(wadahUtama.getContentPane(), BoxLayout.Y_AXIS));
    wadahUtama.setVisible(true);

    JLabel tulisanPembuka = new JLabel("Di depanmu ada sebuah gerbang besar, ini sudah jam 17:00, kau memutuskan untuk masuk ke gerbang tersebut", JLabel.LEFT);
    // tulisanPembuka.setBounds(50, 50, 100, 100);
    wadahUtama.add(tulisanPembuka);

    JButton tombolSuara = new JButton("Wow sepi sekali !");

    tombolSuara.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        System.out.println("Tombol diklik");
        try {
          // Coba load objek suara dan memasukannya ke objek Clip

          // Akses file suara
          File fileContohSuara = new File("./halo-halo.wav");

          // Dapetin objek suara dari file tersebut
          AudioInputStream objekSuara = AudioSystem.getAudioInputStream(fileContohSuara);
          
          // Dapetin objek clip untuk memainkan suara
          Clip objekClip = AudioSystem.getClip();
          objekClip.open(objekSuara);

          objekClip.start();

        } catch (Exception e2) {
        }
      }
    });

    wadahUtama.add(tombolSuara);

  }
}
