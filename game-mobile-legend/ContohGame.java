import java.util.ArrayList;

class Item {
  String name;
  int attackAffected;
  int hpAffected;
  int manaAffected;
  int speedAffected;
  int attackSpeedAffected;

  void usedBy(Hero otherHero) {
    otherHero.basicAttack = otherHero.basicAttack + this.attackAffected;
    otherHero.hp = otherHero.hp + this.hpAffected;
    otherHero.mana = otherHero.mana + this.manaAffected;
    otherHero.speed = otherHero.speed + this.speedAffected;
    otherHero.attackSpeed = otherHero.attackSpeed + this.attackSpeedAffected;
  }
}

class Hero {
  String name;
  int basicAttack;
  int hp;
  int mana;
  int speed;
  int attackSpeed;
  int x = 0;
  int y = 0;
  int gold = 5000;

  ArrayList<Item> items = new ArrayList<Item>();

  void printInfo() {
    System.out.println("Nama: " + this.name 
        + " basicAttack: " + this.basicAttack 
        + " attackSpeed: " + this.attackSpeed
        + " gold: " + this.gold
        + " x: " + this.x
        + " y: " + this.y
        + " hp: " + this.hp
    );
  }

  void buyItem(Item item, int itemPrice) {
    System.out.println("Beli item " + item.name + " senilai " + itemPrice + " gold");
    this.gold = this.gold - itemPrice;
    this.items.add(item);
    item.usedBy(this); 
  }

  void attack(Hero otherHero) {
    int initialHp = otherHero.hp;
    otherHero.hp = otherHero.hp - this.basicAttack;
    System.out.println(this.name + " menyerang " + otherHero.name + " sehingga hp nya dari " + initialHp + " jadi " + otherHero.hp);
  }

  void skill1(Hero otherHero) {
    otherHero.hp = otherHero.hp - this.basicAttack;
  }

  void skill2(Hero otherHero) {
    otherHero.hp = otherHero.hp - this.basicAttack;
  }

  void skill3(Hero otherHero) {
    otherHero.hp = otherHero.hp - this.basicAttack;
  }

  void move(int moveX, int moveY) {
    this.x = this.x + moveX;
    this.y = this.y + moveY;
  }
}

public class ContohGame {
  
  public static void main (String[] argumen) {

    Hero h1 = new Hero();
    h1.name = "Dhowi";
    h1.basicAttack = 50;
    h1.hp = 1000;
    h1.mana = 200;
    h1.speed = 20;
    h1.attackSpeed = 20;

    Hero h2 = new Hero();
    h2.name = "Elsa";
    h2.basicAttack = 70;
    h2.hp = 800;
    h2.mana = 2000;
    h2.speed = 15;
    h2.attackSpeed = 10;

    Item item1 = new Item();
    item1.name = "BOD"; 
    item1.attackAffected = 20;
    item1.attackSpeedAffected = 5;

    Item item2 = new Item();
    item2.name = "Swift Boots"; 
    item2.attackAffected = 0;
    item2.attackSpeedAffected = 25;

    h1.printInfo();
    h1.buyItem(item1, 900);
    h1.move(0, -10);

    h2.attack(h1);

    h1.printInfo();
    h1.buyItem(item2, 1900);
    h1.move(-10, -10);
    h1.printInfo();

    h2.attack(h1);
    h2.attack(h1);
    h2.attack(h1);
    h2.attack(h1);
    h2.attack(h1);
    h1.printInfo();
  }

}
