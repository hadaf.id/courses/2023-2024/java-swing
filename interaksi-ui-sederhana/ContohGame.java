import javax.swing.JFrame; 
import javax.swing.JLabel; 

/**
 * Nama class: JButton
 * Package: javax.swing
 * Tanggung jawab: mengelola dan menampilkan tombol 
 */
import javax.swing.JButton; 

/**
 * Nama class: JTextField
 * Package: javax.swing
 * Tanggung jawab: mengelola dan menampilkan isian text 
 */
import javax.swing.JTextField; 

/**
 * Nama class: JPanel
 * Package: javax.swing
 * Tanggung jawab: mengelola dan menampilkan wadah untuk diisi 
 *  berbagai jenis komponen UIseperti JButton, JTextField, dsb.
 */
import javax.swing.JPanel; 

/**
 * Nama class: BoxLayout 
 * Package: javax.swing
 * Tanggung jawab: mengelola bentuk layout BoxLayout 
 *  - BoxLayout menentukan layout arah horizontal atau vertikal
 * Dokumentasi: https://docs.oracle.com/javase/tutorial/uiswing/layout/box.html
 */
import javax.swing.BoxLayout;

/**
 * Nama class: GridBagLayout 
 * Package: javax.swing
 * Tanggung jawab: mengelola bentuk layout GridBagLayout 
 *  - GridBagLayout menentukan layout berdasarkan matriks panjang x tinggi 
 * Dokumentasi: https://docs.oracle.com/javase/tutorial/uiswing/layout/gridbag.html
 *http://fizyka.umk.pl/~jacek/docs/javatutorial/uiswing/layout/gridbagExample.html
 */
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Nama class: Component 
 * Package: java.awt
 * Tanggung jawab: Class dasar dari komponen-komponen UI seperti Button, Canvas, Checkbox, dsb.
 *  - Memiliki sejumlah Static Constant yang dapat digunakan untuk menentukan posisi (Alignment) pada BoxLayout, seperti:
 *    - BOTTOM_ALIGNMENT
 *    - TOP_ALIGNMENT
 *    - CENTER_ALIGNMENT
 *    - RIGHT_ALIGNMENT
 *    - LEFT_ALIGNMENT
 * Dokumentasi: https://courses.cs.washington.edu/courses/cse341/98au/java/jdk1.2beta4/docs/api/java/awt/Component.html
 */
import java.awt.Component;
import java.awt.Dimension;

public class ContohGame {
  public static void main(String[] argumen) {
    JFrame jendelaUtama = new JFrame();
    jendelaUtama.setSize(640, 480);
    jendelaUtama.setVisible(true);

    GridBagLayout layoutUtama = new GridBagLayout();
    GridBagConstraints konfigurasiLayoutUtama = new GridBagConstraints();

    jendelaUtama.setLayout(layoutUtama);
    jendelaUtama.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JPanel wadahJudulToko = new JPanel();
    // wadahJudulToko.setLayout(new BoxLayout(wadahJudulToko, BoxLayout.X_AXIS));
    wadahJudulToko.setBackground(Color.green);

    JLabel tulisanJudul = new JLabel("Toko buka");

    wadahJudulToko.add(tulisanJudul);

    JButton tombolBukaTutupToko = new JButton("Tutup toko !");
    // tombolBukaTutupToko.setAlignmentX(Component.CENTER_ALIGNMENT);
    // tombolBukaTutupToko.setAlignmentY(Component.CENTER_ALIGNMENT);
    wadahJudulToko.add(tombolBukaTutupToko);

    konfigurasiLayoutUtama.gridx = 0;
    konfigurasiLayoutUtama.gridy = 0;
    konfigurasiLayoutUtama.gridwidth = 1;
    jendelaUtama.add(wadahJudulToko, konfigurasiLayoutUtama);

    JPanel wadahPembelianPermen = new JPanel();
    // wadahPembelianPermen.setLayout(new BoxLayout(wadahPembelianPermen, BoxLayout.X_AXIS));
    wadahPembelianPermen.setBackground(Color.yellow);

    JTextField isianPermen = new JTextField("0");
    isianPermen.setColumns(3);
    isianPermen.setMaximumSize(isianPermen.getPreferredSize());
    wadahPembelianPermen.add(isianPermen);

    JLabel tulisanPermen = new JLabel("permen");
    wadahPembelianPermen.add(tulisanPermen);

    JButton tombolBeliPermen = new JButton("Beli");
    wadahPembelianPermen.add(tombolBeliPermen);

    JLabel tulisanHargaPermen = new JLabel("Total Rp.");
    wadahPembelianPermen.add(tulisanHargaPermen);

    konfigurasiLayoutUtama.gridx = 0;
    konfigurasiLayoutUtama.gridy = 1;
    konfigurasiLayoutUtama.gridwidth = 1;
    jendelaUtama.add(wadahPembelianPermen, konfigurasiLayoutUtama);

    tombolBeliPermen.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int jumlahPermenDipesan = Integer.parseInt(isianPermen.getText()); 
        int hargaPermenSatuan = 500;
        int totalHargaPermen = jumlahPermenDipesan * hargaPermenSatuan;
        tulisanHargaPermen.setText("Total Rp. " + String.valueOf(totalHargaPermen));
      }
    });

    tombolBukaTutupToko.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if(tulisanJudul.getText() == "Toko tutup") {
          tulisanJudul.setText("Toko buka");
          tombolBukaTutupToko.setText("Tutup toko !");
          wadahPembelianPermen.setVisible(true);
        } else {
          tulisanJudul.setText("Toko tutup");
          tombolBukaTutupToko.setText("Buka toko !");
          wadahPembelianPermen.setVisible(false);
        }
      }
    });
  }
}

/**
 * Referensi:
 * - https://www.tutorialsfield.com/jbutton-click-event/
 * - https://stackoverflow.com/questions/10158391/width-in-box-layout
 * - https://stackoverflow.com/questions/39688443/sometimes-swing-text-field-appears-sometimes-it-doesnt
 * - https://docs.oracle.com/javase/tutorial/uiswing/layout/visual.html
 */
