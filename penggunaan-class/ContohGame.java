/**
 * Object Oriented Programming (OOP)
 * Seluruh kebutuhan di dalam aplikasi direpresentasikan sebagai Object
 * Contoh mahasiswa, mata kuliah, postingan, match, pemesanan, driver, row, icon, menu, dsb.
 */

/**
 * Class adalah cetak biru dari Object yang mewakili Entitas yang digunakan pada sistem yang dibuat
 * yang memiliki Attribute / sifat yang melekat ke Class / Object tersebut
 * dan memiliki Method / aksi yang dapat dilakukan dari Class / Object tersebut
 */

class Televisi {

  int ukuran; // Attribute ukuran, satuannya dalam inch
  int tebal; // Attribute tebal, satuannya mm
  String warna; // Attribute warna
  int daya; // Attribute daya, satuannya watt
  String pemilik; // Attribute label dari pemilik televisi ini

  boolean isNyala = false; // Attribute isNyala, TV on atau off, true = on, false = off
  String channelDipilih; // Attribute channelDipilih
  int volumeSuara; // Attribute volumeSuara

  /**
   * Membuat Constructor untuk Class Televisi 
   * Constructor adalah method yang langsung dijalankan
   * begitu Object baru dari Class dibuat
   */
  Televisi (String inputPemilik, String inputWarna) {

    /**
     * this.pemilik : merujuk ke Attribute pemilik dari Object ini (this)
     * inputPemilik : merujuk ke parameter input ke 1 dari Constructor ini
     */
    // Attribute pemilik dari Object ini, akan diisi oleh nilai parameter inputPemilik
    this.pemilik = inputPemilik;

    // Attribute warna dari Object ini, akan diisi oleh nilai parameter inputWarna
    this.warna = inputWarna;
  }

  String getPemilik() {
    // keyword this merujuk kepada Object Televisi
    return this.pemilik;
  }

  /**
   * Method pindahChannelSebelumnya()
   * void artinya method ini tidak mengembalikan nilai apapun
   */
  void pindahChannelSebelumnya() {
  }

  /**
   * Method pindahChannelSelanjutnya()
   */
  void pindahChannelSelanjutnya() {
  }

  /**
   * Method toggleTombolNyala()
   */
  void toggleTombolNyala() {

    /**
     * Tanda ! artinya negasi / kebalikan
     * Artinya, Attribute isNyala akan false, jika isNyala sebelumnya true
     * Attribute isNyala akan true, jika isNyala sebelumnya false 
     */
    this.isNyala = !this.isNyala;

    if(this.isNyala == true) {
      System.out.println("Televisi menyala");
    } else {
      System.out.println("Televisi mati");
    }
  }

  /**
   * Method kecilkanVolumeSuara()
   */
  void kecilkanVolumeSuara() {

    if(this.isNyala == false) {
      System.out.println("TV nya mati, ga bisa ngapa-ngapain");

      // Return, akan menggagalkan eksekusi baris-baris berikutnya pada Method ini
      return;
    }

    // Logikanya, volume suara, bisa dikecilkan ketika lebih dari 0
    // Jika volumenya 0 maka tidak dilakukan apa apa
    if(this.volumeSuara > 0) {
      // volumeSuara sesudah, = volumeSuara sebelum - 1
      this.volumeSuara = this.volumeSuara - 1; 
      System.out.println("Volume suara berubah menjadi " + this.volumeSuara);
    } else {
      System.out.println("Volume suara tidak bisa dikecilkan lagi");
    }
  }

  /**
   * Method besarkanVolumeSuara()
   */
  void besarkanVolumeSuara() {
    // Logikanya, volume suara, bisa dibesarkan ketika kurang dari 100 
    // Jika volumenya 100 maka tidak dilakukan apa apa

    if(this.isNyala == false) {
      System.out.println("TV nya mati, ga bisa ngapa-ngapain");

      // Return, akan menggagalkan eksekusi baris-baris berikutnya pada Method ini
      return;
    }

    if(this.volumeSuara < 100) {
      // volumeSuara sesudah, = volumeSuara sebelum + 1
      this.volumeSuara = this.volumeSuara + 1; // Bisa juga ditulis this.volumeSuara ++
      System.out.println("Volume suara berubah menjadi " + this.volumeSuara);
    } else {
      System.out.println("Volume suara tidak bisa dibesarkan lagi");
    }
  }

}

public class ContohGame {
  public static void main(String[] inputanArgumen) {

    // Membuat Object baru dari Class Televisi yang disimpan dalam variable televisi1
      // Televisi televisi1 : artinya variable televisi1 tipe datanya Televisi
      // Televisi televisi1 = X : artinya memasukan nilai X kedalam variable televisi1 / assignment
      // new Televisi() : artinya Object baru dari Class Televisi
    Televisi televisi1 = new Televisi("Azka", "Biru");
    System.out.println("Pemilik televisi1 adalah " + televisi1.getPemilik());

    Televisi batagor = new Televisi("Syafiq", "Ungu");
    System.out.println("Pemilik batagor adalah " + batagor.getPemilik());

    Televisi televisi4 = new Televisi("Jashima", "Perak");
    System.out.println("Pemilik televisi4 adalah " + televisi4.getPemilik());

    Televisi tv2 = new Televisi("Irham", "Merah");
    System.out.println("Pemilik tv2 adalah " + tv2.getPemilik());

    tv2.toggleTombolNyala();

    tv2.besarkanVolumeSuara();
    tv2.besarkanVolumeSuara();
    tv2.besarkanVolumeSuara();
    tv2.besarkanVolumeSuara();

    tv2.toggleTombolNyala();

    tv2.besarkanVolumeSuara();
    tv2.besarkanVolumeSuara();
    tv2.besarkanVolumeSuara();
  }
}
