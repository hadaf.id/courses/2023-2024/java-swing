/**
 * Nama class: ArrayList 
 * Package: java.util 
 * Tanggung jawab: mengelola dan menampilkan array dinamis 
 */
import java.util.ArrayList;

// Class sekaligus tipe data custom  
class Icon {
  
  // Mendefinisikan attribute dari Class Icon, tipe data String, nama: tipe
  String tipe;

  // Mendefinisikan attribute dari Class Icon, tipe data int[], nama: posisi, jumlah anggota: 2 
  int[] posisi = new int[2];

  // Mendefinisikan attribute dari Class Icon, tipe data boolean, nama: aktif, nilai awal: false 
  boolean aktif = false;
}

public class ContohGame {
  public static void main(String[] argumen2) {

    /**
     * Array tipe primitif, statik
     * int, byte, short, long, float, double, char, boolean
     */

    float beratBadan = 52.4f;
    System.out.println(beratBadan);

    String ucapanSelamat = "Selamat Saum";
    System.out.println(ucapanSelamat);

    // Isi ditentukan ketika pendefinisian
    // Pendefinisian memberitahu jumlah isi anggotanya, dan isi anggotanya
    // tanda [] pada penulisan tipe data int petak1[]
      // artinya array yang dibuat berdimensi 1
      // jika ingin menambah dimensinya, maka ditambahkan tanda [] nya
    int petak1[] = {0, 0, 0, 4, 1}; // 0, 1, 2, 3, 4

    // Mengubah nilai data indeks ke 4 dari 1 menjadi 15
    petak1[4] = 15;

    // Mengakses nilai data indeks ke 4 dari variable petak1
    System.out.println("Isi dari petak1[4] adalah " + petak1[4]);

    // // Isi tidak ditentukan ketika pendefinisian
    // // Pendefinisian hanya memberitahu jumlah isi anggotanya
    int petak2[] = new int[5]; // {0, 0, 0, 0, 0}
  
    // Mengubah nilai data indeks ke 3 dari 0 menjadi 12 
    petak2[3] = 12;

    // Mengakses nilai data indeks ke 3 dari variable petak2
    System.out.println("Isi dari petak2[3] adalah " + petak2[3]);

    // Mendefinisikan array 2 dimensi petak3 yang memiliki 5 anggota
      // dimana setiap anggota, memiliki 3 anggota
      // dimana jumlah anggota dan nilai dari setiap anggota, 
      // ditentukan ketika pendefinisian
    int petak3[][] = {
      {0, 0, 0}, 
      {0, 4, 5},
      {0, 0, 0},
      {0, 0, 1},
      {0, 0, 0},
    };

    // Mengakses nilai data indeks ke 1 dari petak3 : petak3[1]
      // lalu mengakses nilai data indeks ke 2 dari petak3[1]
      // petak3[1][2]
    System.out.println("Isi dari petak3[1][2] adalah " + petak3[1][2]);

    // Mendefinisikan array nilaiUjian1 yang memiliki 3 anggota
      // dimana jumlah anggota dan nilai dari setiap anggota, 
      // ditentukan ketika pendefinisian
    float nilaiUjian1[] = {7.5f, 3.6f, 8.8f}; 

    // Mengakses nilai data indeks ke 1 dari nilaiUjian1
    System.out.println("Isi dari nilaiUjian1[1] adalah " + nilaiUjian1[1]);

    // Mendefinisikan array jawaban1 yang memiliki 4 anggota
      // dimana jumlah anggota dan nilai dari setiap anggota, 
      // ditentukan ketika pendefinisian
    boolean jawaban1[] = {true, false, false, true};

    // Mengakses nilai data indeks ke 3 dari jawaban1
    System.out.println("Isi dari jawaban1[3] adalah " + jawaban1[3]);

    /**
     * Array tipe primitif, dinamis 
     * dibantu Class ArrayList
     */

    // Mendefinisikan array tipe data Integer, dengan nama variable nilai1
      // dimana jumlah anggota belum ditentukan di awal
      // jumlah anggotanya 0
    ArrayList<Integer> nilai1 = new ArrayList<Integer>(); // []

    System.out.println("Jumlah anggota array nilai1 saat ini adalah " + nilai1.size()); 

    nilai1.add(5); // Menambahkan 1 anggota, nilai data = 5, arraynya sekarang [5]
    System.out.println("Anggota array nilai1 saat ini adalah " + nilai1); 
    nilai1.add(7); // Menambahkan 1 anggota, nilai data = 7, arraynya sekarang [5, 7]
    System.out.println("Anggota array nilai1 saat ini adalah " + nilai1); 
    nilai1.add(8); // Menambahkan 1 anggota, nilai data = 8, arraynya sekarang [5, 7, 8]
    System.out.println("Anggota array nilai1 saat ini adalah " + nilai1); 

    System.out.println("Nilai di indeks ke 2 adalah " + nilai1.get(2)); // Ambil nilai di indeks ke 2
    System.out.println("Jumlah anggota array nilai1 saat ini adalah " + nilai1.size()); 

    // System.out.println(nilai1); // Print langsung anggota2nya
                                
    // System.out.println(nilai1.contains(1));

    // Menghapus anggota indeks ke 1 
    // [5, 7, 8] maka indeks ke 1 nya adalah 7
    nilai1.remove(1);

    // Maka array nilai1 sekarang adalah [5, 8]
    System.out.println("Anggota array nilai1 sekarang adalah " + nilai1);
    
    /**
     * Array tipe non primitif, statik 
     * dibantu Class ArrayList
     */
    String[] warga1 = {"Rizal", "Gina"};
    System.out.println(warga1[1]);

    String[] warga2 = new String[2];

    /**
     * Array tipe non primitif, dinamis 
     * dibantu Class ArrayList
     */

    // Mendefinisikan array tipe data String, dengan nama variable penumpangAngkot 
      // dimana jumlah anggota belum ditentukan di awal
      // jumlah anggotanya 0
    ArrayList<String> penumpangAngkot = new ArrayList<String>(); 

    penumpangAngkot.add("Yahye");
    penumpangAngkot.add("Rizal");
    penumpangAngkot.add("Gina");

    // Method contains("") mengecek keberadaan anggota berdasarkan nilainya
    // Mengecek apakah String Ade, ada pada array penumpangAngkot
    System.out.println("Cek apakah Ade merupakan anggota penumpangAngkot " + penumpangAngkot.contains("Ade"));

    // Mendefinisikan array tipe data buatan sendiri: Icon, dengan nama variable kumpulanIcon 
      // dimana jumlah anggota belum ditentukan di awal
      // jumlah anggotanya 0
    ArrayList<Icon> kumpulanIcon = new ArrayList<Icon>();

    // Menambahkan Object dari Icon ke dalam array kumpulanIcon
    kumpulanIcon.add(new Icon(){{
      tipe = "gula";
      posisi[0] = 5;
      posisi[1] = 2;
      aktif = true;
    }});

    // Menambahkan Object dari Icon ke dalam array kumpulanIcon
    kumpulanIcon.add(new Icon(){{
      tipe = "olahraga";
      posisi[0] = 8;
      posisi[1] = 7;
      aktif = true;
    }});

    // Looping terhadap array kumpulanIcon
    // kumpulanIcon.size() adalah method untuk menentukan jumlah anggota
    for(int i = 0; i < kumpulanIcon.size(); i++) {
      System.out.println(kumpulanIcon.get(i).tipe);
    }

  }
}
