/**
 * Nama class: JFrame()
 * Package: javax.swing
 * Tanggung jawab: mengelola dan menampilkan jendela utama aplikasi 
 */
import javax.swing.JFrame; 

/**
 * Nama class: JLabel(p1)
 * Package: javax.swing
 * Tanggung jawab: mengelola dan menampilkan label tulisan
 * Parameter:
 *  1. Isi tulisan label
 */
import javax.swing.JLabel; 

/**
 * Nama class: FlowLayout(p1)
 * Package: java.awt
 * Tanggung jawab: mengelola dan menampilkan jenis layout FlowLayout 
 * Parameter:
 *  1. Arah susunan elemen-elemen UI pada layout tersebut 
 *    - FlowLayout.LEFT
 *    - FlowLayout.CENTER
 *    - FlowLayout.RIGHT
 */
import java.awt.FlowLayout; 

public class ContohGame {
  public static void main(String[] argumen) {

    /**
     * Script di bawah ini: Membuat object JFrame, disimpan di variable jendela1 
     */
    JFrame jendela1 = new JFrame();
    
    /**
     * Nama method: setSize(p1, p2) 
     * Kepunyaan class: JFrame
     * Tanggung jawab: menentukan ukuran jendela 
     * Parameter:
     *  1. lebar jendela 
     *  2. tinggi jendela
     *
     * Script di bawah ini: lebar jendela 640 pixel, tinggi 480 pixel 
     */
    jendela1.setSize(640, 480);

    /**
     * Nama method: setVisible(p1) 
     * Kepunyaan class: JFrame
     * Tanggung jawab: menentukan jendelanya ditampilkan atau nggak 
     * Parameter:
     *  1. true: jendela ditampilkan, false: jendela tidak ditampilkan 
     *
     * Script di bawah ini: tampilkan jendelanya 
     */
    jendela1.setVisible(true);

    /**
     * Nama method: setDefaultCloseOperation(p1) 
     * Kepunyaan class: JFrame
     * Tanggung jawab: menentukan kondisi ketika tombol x / close dari jendela diklik 
     * Parameter:
     *  1. Ketika tombol close diklik mau diapain ?
     *      - JFrame.EXIT_ON_CLOSE: keluar aplikasi 
     *      - JFrame.HIDE_ON_CLOSE: sembunyikan aplikasi
     *      - JFrame.DISPOSE_ON_CLOSE: objek frame nya aja yang dihapus, aplikasi tetap jalan
     *      - JFrame.DO_NOTHING_ON_CLOSE: ga ngapa ngapain
     *
     * Script di bawah ini: jika tombol exit di klik, aplikasi selesai 
     */
    jendela1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    /**
     * Nama method: setLayout(p1) 
     * Kepunyaan class: JFrame
     * Tanggung jawab: menentukan jenis layout yang digunakan: FlowLayout / BoxLayout / GridLayout / dsb. 
     * Parameter:
     *  1. object dari class layout yang digunakan 
     *
     * Script di bawah ini: menggunakan FlowLayout sebagai jenis layout yang digunakan
     */
    jendela1.setLayout(new FlowLayout(FlowLayout.CENTER));
    
    /**
     * Script di bawah ini: membuat object JLabel, disimpan di variable tulisan1
     */
    JLabel tulisan1 = new JLabel("Ini Game yang gak seru tapi penuh hikmah");

    /**
     * Nama method: add(p1) 
     * Kepunyaan class: JFrame
     * Tanggung jawab: menambahkan objek elemen UI seperti JLabel, JButton, dsb. ke jendela utama 
     * Parameter:
     *  1. objek elemen UI seperti objek dari class JLabel, JButton, dsb. 
     */
    jendela1.add(tulisan1);

  }
}
