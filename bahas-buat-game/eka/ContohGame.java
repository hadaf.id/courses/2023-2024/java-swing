import java.util.ArrayList;
import java.util.Random;

public class ContohGame {

  Pemain pemain; // Attribute dari Object ContohGame
  ArrayList<Tanaman> kumpulanTanaman = new ArrayList<Tanaman>();
  ArrayList<Hama> kumpulanHama = new ArrayList<Hama>();

  // Constructor
  ContohGame () {
    System.out.println("Game dimulai");

    this.pemain = new Pemain(this);

    this.kumpulanTanaman.add(new Tanaman(){{
      tipe = "KOL";
      posisi[0] = 10;
      posisi[1] = 10;
    }});

    this.kumpulanTanaman.add(new Tanaman(){{
      tipe = "WORTEL";
      posisi[0] = 20;
      posisi[1] = 10;
    }});

    this.kumpulanTanaman.add(new Tanaman(){{
      tipe = "BAWANG";
      posisi[0] = 30;
      posisi[1] = 10;
    }});

    this.kumpulanTanaman.add(new Tanaman(){{
      tipe = "CABAI";
      posisi[0] = 10;
      posisi[1] = 20;
    }});

    int jumlahAwalHama = 10;

    for(int i = 0; i < jumlahAwalHama; i++) {

      Hama hamaBaru = new Hama();

      Random mesinRandom = new Random();

      hamaBaru.posisi[0] = mesinRandom.nextInt(20);
      hamaBaru.posisi[1] = mesinRandom.nextInt(20);

      this.kumpulanHama.add(hamaBaru);
      
      // System.out.println("Buat hama ke " + (i + 1) + ", X: " + hamaBaru.posisi[0] + " Y: " + hamaBaru.posisi[1]);
    }

    this.pemain.lihatPosisi();
    this.pemain.jalan(1,0);
    this.pemain.lihatPosisi();
  }

  public static void main(String[] args) {

    ContohGame game = new ContohGame();

  }
}
