class Pemain {

  ContohGame gameIni;

  Pemain (ContohGame gameIni) {
    this.gameIni = gameIni;
  }

  // Attribute
  int[] posisi = new int[]{19, 10}; // {x, y} 

  // Method
  void lihatPosisi () {
    System.out.println("Posisi sekarang x: " + this.ambilX() + ", y: " + this.ambilY());
  }

  void jalan (int langkahX, int langkahY) {
    this.posisi[0] = this.posisi[0] + langkahX;
    this.posisi[1] = this.posisi[1] + langkahY;

    this.aksiBertemuObjekLain();
  }

  void aksiBertemuObjekLain() {
    System.out.println("aksiBertemuObjekLain()");

    for( int i = 0; i < this.gameIni.kumpulanTanaman.size(); i++ ) {

      System.out.println("Cek tanaman " + this.gameIni.kumpulanTanaman.get(i).tipe);
      System.out.println("posisi X " + this.gameIni.kumpulanTanaman.get(i).posisi[0]);
      System.out.println("posisi Y " + this.gameIni.kumpulanTanaman.get(i).posisi[1]);

      System.out.println("posisi X pemain " + this.posisi[0]);
      System.out.println("posisi Y pemain " + this.posisi[1]);

      if(this.posisi[0] == this.gameIni.kumpulanTanaman.get(i).posisi[0]
        && this.posisi[1] == this.gameIni.kumpulanTanaman.get(i).posisi[1]
          ) {
        System.out.println("WOW KETEMU!");
      }
    }


    // Pemain bertemu tanaman
    
    // Pemain bertemu hama
  }

  int ambilX () { return this.posisi[0]; }
  int ambilY () { return this.posisi[1]; }
}

