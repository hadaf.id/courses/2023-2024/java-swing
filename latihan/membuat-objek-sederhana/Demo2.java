class Manusia {
  int uang;
}

class Makanan {
  int gizi = 0;
  String nama = "Makanan";
  void getInfo() {
    System.out.println(this.nama + " punya gizi " + this.gizi);
  }
}

class NasiGoreng extends Makanan {
  int gizi = 1;
}

class PenjualNasiGoreng extends Manusia {

  int penjualan = 0;

  int persediaanNasiGram = 2000;
  int persediaanBumbuGram = 500;
  int persediaanTelurButir = 300;

  void siapkanPenggorengan() {
    // nyalakan kompor
    // simpan katel
    // masukkan minyak
  }

  void masukkanBumbu(int jumlahGram) {
  }

  void masukkanTelur(int jumlahButir) {
  }

  void masukkanNasi(int jumlahGram) {
    // masak nasi
    // masukkan nasi ke wadah nasi
  }

  void adukRata(int menit) {
  }

  NasiGoreng buat() {

    this.siapkanPenggorengan();
    this.masukkanBumbu();
    this.adukRata(2);

    this.masukkanTelur();
    this.adukRata(3);

    this.masukkanNasi(200);
    this.adukRata(5);

    NasiGoreng nasiGoreng = new NasiGoreng();
    return nasiGoreng;
  }
}

public class Demo2 {
  public static void main(String[] argumen) {

  }
}
