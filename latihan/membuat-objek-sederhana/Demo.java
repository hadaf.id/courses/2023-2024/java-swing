import java.util.ArrayList;

class Utang {
  int jumlah;
  Manusia pemberiPinjam;
  int waktuPinjam;
}

class Manusia {
  String nama;
  int uang;
  ArrayList<Utang> listUtang = new ArrayList<Utang>();

  void pinjamkanUang(Manusia manusiaLain, int uangDipinjamkan) {

    if(this.uang > uangDipinjamkan) {

      this.uang -= uangDipinjamkan;
      manusiaLain.uang += uangDipinjamkan;

      Utang utang = new Utang();
      utang.jumlah = uangDipinjamkan;
      utang.pemberiPinjam = this; // this merujuk ke objek Manusia ini

      manusiaLain.listUtang.add(utang);

      System.out.println(this.nama + " meminjamkan uang ke " + manusiaLain.nama);
      System.out.println("Uangnya " + this.nama + " jadi " + this.uang);
      System.out.println("Uangnya " + manusiaLain.nama + " jadi " + manusiaLain.uang);
      System.out.println("Sehingga list utang " + manusiaLain.nama + " menjadi: ");

      for(int i = 0; i < manusiaLain.listUtang.size(); i++ ){
        System.out.println("Utang ke " + manusiaLain.listUtang.get(i).pemberiPinjam.nama + " senilai: " + manusiaLain.listUtang.get(i).jumlah);
      }
    } else {
      System.out.println("Tidak bisa meminjamkan uang, karena uangnya " + this.nama + " hanya " + this.uang);
    }


  }
}

public class Demo {
  public static void main(String[] argumen) {

    Manusia manusia1 = new Manusia();
    manusia1.uang = 100000;
    manusia1.nama = "Yildi";

    Manusia manusia3 = new Manusia();
    manusia3.uang = 50000;
    manusia3.nama = "Fikri";

    Manusia manusia2 = new Manusia();
    manusia2.uang = 3000;
    manusia2.nama = "Ikhsan";

    manusia1.pinjamkanUang(manusia2, 10000);
    manusia1.pinjamkanUang(manusia2, 7000);
    manusia3.pinjamkanUang(manusia2, 9000);
    manusia1.pinjamkanUang(manusia2, 41000);
    manusia3.pinjamkanUang(manusia2, 30000);
    manusia1.pinjamkanUang(manusia2, 20000);

    // Disini, gimana kita bisa tau, siapa minjem ke siapa ?

  }
}
