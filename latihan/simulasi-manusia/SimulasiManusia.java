class Organ {
  int kesehatan = 80;
  String nama = "";

  void printKesehatan () {
    System.out.println("Kesehatan organ " 
      + this.nama 
      + " = " 
      + this.kesehatan 
      + " %"
    );
  }
}

class Jantung extends Organ {
  Jantung() {
    this.nama = "Jantung";
  }
}

class Paru extends Organ {
  Paru() {
    this.nama = "Paru-paru";
  }
}

class Mata extends Organ {
  Mata() {
    this.nama = "Mata";
  }
}

class Lambung extends Organ {
  Lambung() {
    this.nama = "Lambung";
  }
}

class GrupOrgan {
  Jantung jantung = new Jantung();
  Paru paru = new Paru();
  Mata mata = new Mata();
  Lambung lambung = new Lambung();

  void printInfo() {
    this.jantung.printKesehatan();
    this.paru.printKesehatan();
    this.mata.printKesehatan();
    this.lambung.printKesehatan();
  }
}

class Makanan {
  String nama = "";
  int untukMata = 0;
  int untukJantung = 0;
  int untukParu = 0;
  int untukLambung = 0;
  void mempengaruhi(Manusia manusia) {
    manusia.organ.mata.kesehatan += this.untukMata;
    manusia.organ.jantung.kesehatan += this.untukJantung;
    manusia.organ.paru.kesehatan += this.untukParu;
    manusia.organ.lambung.kesehatan += this.untukLambung;
  }
}

class Burger extends Makanan {
  Burger () {
    this.nama = "Burger";
    this.untukMata = 0;
    this.untukJantung = -2;
    this.untukParu = 0;
    this.untukLambung = 1;
  }
}

class Salad extends Makanan {
  Salad () {
    this.nama = "Salad";
    this.untukMata = 2;
    this.untukJantung = 1;
    this.untukParu = 1;
    this.untukLambung = 1;
  }
}

class Manusia {
  GrupOrgan organ = new GrupOrgan();
  String nama;

  void makan(Makanan makanan) {
    System.out.println(this.nama + " makan " + makanan.nama);
    makanan.mempengaruhi(this); 
    this.printInfo();
  }

  void printInfo() {
    System.out.println("Nama nya " + this.nama);
    this.organ.printInfo();
  }
}

class Mahasiswa extends Manusia {
}

public class SimulasiManusia {

  public static void main(String[] argumen) {
    
    Mahasiswa mahasiswa1 = new Mahasiswa();
    mahasiswa1.nama = "James";
    mahasiswa1.printInfo();

    Makanan burger1 = new Burger();
    mahasiswa1.makan(burger1);

    Makanan salad1 = new Salad();
    mahasiswa1.makan(salad1);
    mahasiswa1.makan(salad1);
    mahasiswa1.makan(salad1);
    mahasiswa1.makan(salad1);
    mahasiswa1.makan(salad1);
    mahasiswa1.makan(salad1);
  }
}
