/**
PERHATIAN !!! 
JANGAN ASAL COPY PASTE KODE-KODE DI HALAMAN INI,
KECUALI DIPAHAMI TERLEBIH DAHULU MAKSUDNYA
TIAP KODE SUDAH DIBERI PENJELASAN, BACA DULU YES !
*/

/**
 * Nama class: JPanel
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan wadah untuk menampung berbagai komponen User Interface seperti JButton, JLabel, JTextField, dsb. 
 */
import javax.swing.JPanel; 

/**
 * Nama class: JLabel
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan label seperti text ataupun gambar 
 */
import javax.swing.JLabel; 

/**
 * Nama class: JButton
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan tombol 
 */
import javax.swing.JButton; 

/**
 * Nama class: JTextField
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan form isian 
 */
import javax.swing.JTextField;

/**
 * Nama class: ImageIcon 
 * Package: javax.swing
 * Tanggung jawab: Merepresentasikan Icon berupa Image
 */
import javax.swing.ImageIcon;

/**
 * Nama class: BoxLayout 
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan layout bertipe BoxLayout 
 *  BoxLayout dapat mengatur layout secara horizontal maupun vertikal
 */
import javax.swing.BoxLayout;

/**
 * Nama class: EmptyBorder 
 * Package: javax.swing.border
 * Tanggung jawab: Mengelola dan menampilkan border kosong yang dapat bertindak sebagai area kosong di sekeliling komponen
 */
import javax.swing.border.EmptyBorder;

/**
 * Nama class: LineBorder 
 * Package: javax.swing.border
 * Tanggung jawab: Mengelola dan menampilkan border dengan tebal dan warna tertentu di sekeliling komponen 
 */
import javax.swing.border.LineBorder;

/**
 * Nama class: Box 
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan komponen tidak terlihat yang dapat digunakan sebagai pengisi jarak di antara komponen 
 *  Mirip dengan mengisi ruang antar rumah-rumah di RT 07 yang saling membelakangi dengan selokan
 */
import javax.swing.Box;

/**
 * Nama class: SwingConstants 
 * Package: javax.swing
 * Tanggung jawab: Berisi kumpulan konstanta yang umum digunakan pada perlayoutan komponen 
 *  Contoh: SwingConstants.CENTER, SwingConstants.NORTH, dsb.
 */
import javax.swing.SwingConstants;

/**
 * Nama class: GridLayout 
 * Package: java.awt
 * Tanggung jawab: Mengelola dan menampilkan layout bertipe GridLayout 
 *  yang memudahkan membuat layout seperti matriks atau tabel dengan sejumlah baris dan kolom
 *  dapat digunakan untuk membuat layout seperti tombol handphone, papan catur, menu-menu Icon, dsb.
 */
import java.awt.GridLayout;

/**
 * Nama class: Color 
 * Package: java.awt
 * Tanggung jawab: Mengelola dan merepresentasikan warna untuk berbagai kebutuhan 
 */
import java.awt.Color;

/**
 * Nama class: ActionListener
 * Package: java.awt.event
 * Tanggung jawab: memantau event dan mendelegasikan aksi ketika event tersebut terjadi
 */
import java.awt.event.ActionListener;

/**
 * Nama class: ActionEvent
 * Package: java.awt.event
 * Tanggung jawab: merepresentasikan event aksi dari komponen
 */
import java.awt.event.ActionEvent;

/**
 * Nama class: Dimension 
 * Package: java.awt
 * Tanggung jawab: merepresentasikan ukuran 
 */
import java.awt.Dimension;

class TampilanDashboard extends JPanel {

  /**
   * Attribute gameIni merepresentasikan object ContohGame
   * Mengapa dijadikan attribute dari Class TampilanDashboard ?
   *  Agar dapat diakses dari berbagai method dan Class turunan dari Class ini 
   */
  ContohGame gameIni;

  /**
   * Di bawah ini kita menggunakan Constructor dari Class TampilanPesanMakanan
   * Constructor adalah sekumpulan perintah yang dijalankan ketika Class tersebut diinisiasi
   *
   * Class TampilkanDashboard ini dibuat dengan sebuah parameter berisi Object bertipe ContohGame
   *  object ini akan digunakan agar dapat mengakses berbagai method pada object ContohGame 
   *  seperti pindahMenu()
   *
   * Penjelasan tentang Class, Object, Method dan Attribute: 
   * Penjelasan tentang Constructor: 
   */
  TampilanDashboard(ContohGame gameIni) {

    this.gameIni = gameIni;

    /**
     * Nama method: setLayout(p1) 
     * Kepunyaan class: JFrame, JPanel
     * Tanggung jawab: Menentukan pengelola layout dari object TampilanDashboard (JPanel) 
     * Parameter:
     *  1. Object dari pengelola layout yang digunakan, contohnya: CardLayout, BoxLayout, GridLayout, dsb. 
     *
     * Script di bawah ini: Gunakan BoxLayout dengan arah vertikal sebagai layout untuk Class ini 
     */
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    /**
     * Nama method: setBackground(p1) 
     * Kepunyaan class: JFrame, JPanel
     * Tanggung jawab: Menentukan warna background dari JPanel ini 
     * Parameter:
     *  1. Object Color 
     *
     * Script di bawah ini: Ubah Background menjadi warna putih 
     *  Color.decode("#ffffff") mengubah String kode warna HEX menjadi object Color
     */
    this.setBackground(Color.decode("#ffffff"));

    /**
     * Class WadahPencarian dibuat untuk mewakili bagian atas dari aplikasi yang digunakan untuk pencarian
     *
     * Instansiasi Object dari Class WadahPencarian yang diwadahi variable wadahPencarian
     *  WadahPencarian memiliki sebuah parameter yaitu Object dari Class ContohGame
     *  yang akan digunakan untuk navigasi dari komponen-komponen anak cucunya
     */
    JPanel wadahPencarian = new WadahPencarian(this.gameIni);
    /**
     * Menambahkan Object dari Class WadahPencarian ke dalam Class TampilanDashboard (JPanel) ini
     */
    this.add(wadahPencarian);

    /**
     * Instansiasi Object dari Class JPanel yang dibuat sebagai wadah untuk bagian konten dari aplikasi yang berisi untuk berbagai bagaian menu 
     *  seperti dompet, aneka menu, dan berita terbaru
     */
    JPanel wadahKonten = new JPanel();

    wadahKonten.setBorder(new LineBorder(Color.decode("#ffffff"), 10));
    wadahKonten.setLayout(new BoxLayout(wadahKonten, BoxLayout.Y_AXIS));

    JPanel wadahDompet = new WadahDompet(this.gameIni);
    wadahKonten.add(wadahDompet);
    this.add(wadahKonten);

    JPanel wadahKumpulanMenu = new WadahKumpulanMenu(this.gameIni);
    wadahKonten.add(wadahKumpulanMenu);
    this.add(wadahKumpulanMenu);

  }

  /**
   * Nama class: WadahPencarian
   * Tanggung jawab: sebagai wadah (ekstensi dari Class JPanel) yang menampilkan kotak pencarian serta tombol profil 
   */
  class WadahPencarian extends JPanel {
    WadahPencarian(ContohGame gameIni) {
      this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
      this.setBorder(new EmptyBorder(10, 10, 10, 10));
      this.setBackground(Color.decode("#02860b"));

      /**
       * Membuat tombol dengan Icon
       * Tombol ujung lengkung (rounded button) di Java Swing membutuhkan script custom 
       */
      ImageIcon iconPengguna = new ImageIcon("./icon/pengguna.png");
      JButton tombolPengguna = new JButton(iconPengguna);
      tombolPengguna.setBackground(Color.decode("#ffffff"));
      tombolPengguna.setBorder(new EmptyBorder(10, 10, 10, 10));

      /**
       * Membuat label pencarian berupa Icon dari Image menggunakan JLabel dan ImageIcon
       */
      JLabel labelCari = new JLabel(new ImageIcon("./icon/cari.png"));
      labelCari.setOpaque(true);
      labelCari.setBackground(new Color(255, 255, 255));
      labelCari.setMaximumSize(new Dimension(64, tombolPengguna.getPreferredSize().height));

      /**
       * Membuat isian untuk pencarian menggunakan JTextField 
       */
      JTextField isianPencarian = new JTextField();
      isianPencarian.setBorder(new EmptyBorder(0, 0, 0, 10)); // top, left, bottom, right
      isianPencarian.setMaximumSize(new Dimension(600, tombolPengguna.getPreferredSize().height));

      /**
       * Menambahkan setiap komponen yang telah didefinisikan ke dalam Object ini
       */
      this.add(labelCari);
      this.add(isianPencarian);
      this.add(new Box.Filler(new Dimension(10, 0), new Dimension(10, 0), new Dimension(10, 0)));
      this.add(tombolPengguna);
    }
  }

  /**
   * Nama Class: WadahDompet
   * Tanggung jawab: sebagai wadah (ekstensi dari Class JPanel) yang menampilkan informasi dompet digital 
   */
  class WadahDompet extends JPanel {

    WadahDompet(ContohGame gameIni) {
      this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
      this.setMaximumSize(new Dimension(600, 100));
      this.setBorder(new LineBorder(Color.decode("#00809f"), 10));
      this.setBackground(Color.decode("#00809f"));

      JPanel wadahInfo = new JPanel();
      wadahInfo.setBackground(Color.decode("#ffffff"));
      wadahInfo.setMaximumSize(new Dimension(600, 100));
      wadahInfo.setLayout(new BoxLayout(wadahInfo, BoxLayout.Y_AXIS));
      wadahInfo.setBorder(new LineBorder(Color.decode("#ffffff"), 10));

      wadahInfo.add(new JLabel("cibirupay"));
      wadahInfo.add(new JLabel("Rp 15.900"));
      wadahInfo.add(new JLabel("Lihat catatan"));

      this.add(wadahInfo);
      this.add(new Box.Filler(new Dimension(10, 0), new Dimension(10, 0), new Dimension(10, 0)));

      /**
       * Peringkasan penggunaan Method pada Object baru
       *
       * Manusia manusia = new Manusia()
       * manusia.makan()
       * manusia.lari()
       * manusia.terbang()
       *
       * Kode di atas dapat juga ditulis: 
       *
       * Manusia manusia = new Manusia() {{
       *  makan()
       *  lari()
       *  terbang()
       * }}
       *
       */

      JButton tombolBayar = new JButton("Bayar", new ImageIcon("./icon/bayar-biru.png")) {{
        setVerticalTextPosition(SwingConstants.BOTTOM);
        setHorizontalTextPosition(SwingConstants.CENTER);
        setForeground(Color.decode("#00829f"));
        setBackground(Color.decode("#ffffff"));
        setBorder(new LineBorder(Color.decode("#ffffff"), 10));
      }};

      this.add(tombolBayar);
      this.add(new Box.Filler(new Dimension(10, 0), new Dimension(10, 0), new Dimension(10, 0)));

      JButton tombolTopUp = new JButton("Top Up", new ImageIcon("./icon/tambah-biru.png")) {{
        setVerticalTextPosition(SwingConstants.BOTTOM);
        setHorizontalTextPosition(SwingConstants.CENTER);
        setForeground(Color.decode("#00829f"));
        setBackground(Color.decode("#ffffff"));
        setBorder(new LineBorder(Color.decode("#ffffff"), 10));
      }};

      this.add(tombolTopUp);

    }
  }

  /**
   * Nama Class: WadahKumpulanMenu
   * Tanggung jawab: sebagai wadah (ekstensi dari Class JPanel) yang menampilkan berbagai menu aplikasi di dashboard 
   */
  class WadahKumpulanMenu extends JPanel {

    /**
     * Di bawah ini kita menggunakan Constructor dari Class WadahKumpulanMenu 
     * Constructor adalah sekumpulan perintah yang dijalankan ketika Object dari Class tersebut dibuat
     * Penjelasan tentang Class, Object, Method dan Attribute: 
     * Penjelasan tentang Constructor: 
     */
    WadahKumpulanMenu(ContohGame gameIni) {
      this.setLayout(new GridLayout(0, 4) {{
        setHgap(10);
        setVgap(10);
        setBorder(new EmptyBorder(0, 10, 10, 10));
        setMaximumSize(new Dimension(600, 200));
        setBackground(Color.decode("#ffffff"));
      }});

      /**
       * Menggunakan array untuk mempermudah iterasi 8 menu
       */
      String[] labelMenu = {"MoMotor", "MoMobil", "MoMakan", "MoNgirim", "MoBelanja", "MoPulsa", "Check In", "Lainnya"};
      String[] warnaMenu = {"#dffad1", "#dffad1", "#ffe0df", "#dffad1", "#ffe0df", "#d3f4fb", "#d3f4fb", "#efefef"};

      /**
       * pindahHalamanMenu adalah array berisi string yang mewakili konstanta menu yang dibuat pada ContohGame.java
       */
      String[] pindahHalamanMenu = {
        ContohGame.MENU_PESAN_MOTOR,
        ContohGame.MENU_PESAN_MOBIL,
        ContohGame.MENU_PESAN_MAKANAN,
        ContohGame.MENU_PESAN_MAKANAN,
        ContohGame.MENU_PESAN_MAKANAN,
        ContohGame.MENU_PESAN_MAKANAN,
        ContohGame.MENU_PESAN_MAKANAN,
        ContohGame.MENU_PESAN_MAKANAN,
      };

      /**
       * Meng-iterasi ke 8 menu yang telah dibuat
       */
      for (int urutanKe = 0; urutanKe < labelMenu.length; urutanKe++) {

        final int urutanMenuKe = urutanKe;

        /**
         * Menambahkan tombol ke dalam WadahKumpulanMenu
         *
         * Penjelasan JButton(labelMenu[urutanMenuKe]) :
         *  - JButton butuh parameter String sebagai label tombol
         *  - labelMenu[urutanMenuKe] mengambil String dari Array labelMenu 
         *  - urutanMenuKe mewakili urutan pada labelMenu untuk setiap iterasi
         */
        this.add(new JButton(labelMenu[urutanMenuKe]) {{

          // Menentukan border dari JButton: tanpa border
          setBorder(null);

          // Menentukan background JButton mengikuti urutan pada array warnaMenu
          setBackground(Color.decode(warnaMenu[urutanMenuKe]));

          /**
           * Menambahkan Object pendengar event ActionListener
           *  yang berfungsi mendeteksi Action yang terjadi pada JButton seperti Click
           */
          addActionListener(new ActionListener() {

            /**
             * Menentukan aksi yang terjadi ketika tombol dikenai event Click (diklik)
             */
            public void actionPerformed(ActionEvent e) {

              // Memanggil method pindahMenu() untuk pindah halaman berdasarkan urutan para array pindahHalamanMenu
              gameIni.pindahMenu(pindahHalamanMenu[urutanMenuKe]);
            }
          });
        }});
      }
    }
  }

}
