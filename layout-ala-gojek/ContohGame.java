/**
PERHATIAN !!! 
JANGAN ASAL COPY PASTE KODE-KODE DI HALAMAN INI,
KECUALI DIPAHAMI TERLEBIH DAHULU MAKSUDNYA
TIAP KODE SUDAH DIBERI PENJELASAN, BACA DULU YES !
*/

/**
 * Nama class: JFrame
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan jendela utama aplikasi 
 */
import javax.swing.JFrame; 

/**
 * Nama class: JPanel
 * Package: javax.swing
 * Tanggung jawab: Mengelola dan menampilkan wadah untuk menampung berbagai komponen User Interface seperti JButton, JLabel, JTextField, dsb. 
 */
import javax.swing.JPanel; 

/**
 * Nama class: Color 
 * Package: java.awt
 * Tanggung jawab: Mengelola dan merepresentasikan warna untuk berbagai kebutuhan 
 */
import java.awt.Color; 

/**
 * Nama class: CardLayout
 * Package: java.awt
 * Tanggung jawab: Mengelola layout CardLayout yang dapat digunakan untuk mengelola berbagai tampilan halaman 
 *  Prinsip kerja CardLayout mirip dengan slide presentasi Power Point dimana ada tumpukan presentasi
 *  Ditampilkan satu per satu sesuai slide yang dipilih
 */
import java.awt.CardLayout;

/**
 * Nama class: ContohGame
 * Tanggung jawab: Mengelola, menampilkan, dan menjadi wadah utama dari game / aplikasi
 * Ekstensi dari: JFrame, artinya class ContohGame memiliki Attribute dan Method yang sama dengan JFrame
 *  Ingat kembali penurunan Class,
 *    contohnya Class Mahasiswa ekstensi dari Class Manusia
 *    Class Entog ekstensi dari Class Burung
 *    Class EntogCibiru ekstensi dari Class Entog
 */
public class ContohGame extends JFrame {

  /**
   * Menentukan konstanta-konstanta untuk membedakan tiap halaman dan untuk memudahkan ganti halaman
   *
   * Keyword final pada Java artinya tidak bisa diubah lagi
   * Keyword static pada Java artinya dapat langsung diakses dari Class ContohGame
   */
  final static String MENU_DASHBOARD = "Menu Dashboard";
  final static String MENU_PESAN_MOTOR = "Menu Pesan Motor";
  final static String MENU_PESAN_MOBIL = "Menu Pesan Mobil";
  final static String MENU_PESAN_MAKANAN = "Menu Pesan Makanan";
  final static String MENU_KIRIM_BARANG = "Menu Pesan Kirim Barang";
  final static String MENU_BELANJA = "Menu Belanja";
  final static String MENU_PULSA = "Menu Pulsa";
  final static String MENU_CHECK_IN = "Menu Check In";
  final static String MENU_LAINNYA = "Menu Lainnnya";

  final static int LEBAR_APLIKASI = 360;
  final static int TINGGI_APLIKASI = 640;

  /**
   * Attribute pengelolaMenu bertipe CardLayout
   *  digunakan untuk mengkonfigurasi dan memindahkan halaman CardLayout
   */
  CardLayout pengelolaMenu;

  // Constructor 
  ContohGame() {

    /**
     * Instansiasi Object dari Class CardLayout yang diwadahi oleh Attribute pengelolaMenu
     * Keyword this merujuk ke Object ini
     * this.pengelolaMenu artinya Attribute pengelolaMenu nya object dari Class ContohGame
     */
    this.pengelolaMenu = new CardLayout();

    /**
     * Nama method: setLayout(p1) 
     * Kepunyaan class: JFrame 
     * Tanggung jawab: Menentukan pengelola layout dari object ContohGame (JFrame) 
     * Parameter:
     *  1. Object dari pengelola layout yang digunakan, contohnya: CardLayout, BoxLayout, GridLayout, dsb. 
     *
     * Script di bawah ini: Gunakan this.pengelolaMenu yang berisi object dari CardLayout untuk mengelola ContohGame (JFrame) ini 
     */
    this.setLayout(this.pengelolaMenu);

    /**
     * Nama method: setSize(p1) 
     * Kepunyaan class: JFrame 
     * Tanggung jawab: Menentukan ukuran lebar dan tinggi dari object ContohGame (JFrame) ini 
     * Parameter:
     *  1. Aksi yang dilakukan 
     */
    this.setSize(ContohGame.LEBAR_APLIKASI, ContohGame.TINGGI_APLIKASI);

    /**
     * Nama method: setDefaultCloseOperation(p1) 
     * Kepunyaan class: JFrame 
     * Tanggung jawab: Menentukan apa yang terjadi jika tombol X pada jendela object ContohGame (JFrame) ini diklik 
     * Parameter:
     *  1. Aksi yang dilakukan 
     */
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    /**
     * Nama method: setVisible(p1) 
     * Kepunyaan class: JFrame 
     * Tanggung jawab: Menentukan object ContohGame (JFrame) ini terlihat (true) atau tersembunyi (false) 
     * Parameter:
     *  1. true / false
     */
    this.setVisible(true);

    /**
     * Membuat Object dari Class TampilanDashboard
     * Keyword this merujuk kepada Object dari Class ContohGame ini
     */
    JPanel tampilanDashboard = new TampilanDashboard(this);
    /**
     * Nama method: add(p1, p2) 
     * Kepunyaan class: JFrame 
     * Tanggung jawab: menambahkan komponen ke dalam JFrame (jendela utama) 
     * Parameter:
     *  1. Object dari Class yang ditambahkan (tampilanDashboard)
     *  2. ID dari menu CardLayout nya yaitu konstanta ContohGame.MENU_DASHBOARD
     */
    this.add(tampilanDashboard, ContohGame.MENU_DASHBOARD);
    
    /**
     * Membuat Object dari Class TampilanPesanMotor
     * Keyword this merujuk kepada Object dari Class ContohGame ini
     */
    JPanel tampilanPesanMotor = new TampilanPesanMotor(this);
    /**
     * Nama method: add(p1, p2) 
     * Kepunyaan class: JFrame 
     * Tanggung jawab: menambahkan komponen ke dalam JFrame (jendela utama) 
     * Parameter:
     *  1. Object dari Class yang ditambahkan (tampilanPesanMotor)
     *  2. ID dari menu CardLayout nya yaitu konstanta ContohGame.MENU_PESAN_MOTOR
     */
    this.add(tampilanPesanMotor, ContohGame.MENU_PESAN_MOTOR);

    /**
     * Penjelasan dua yang di bawah sama dengan yang di atas, tidak usah diulang ya
     */
    
    JPanel tampilanPesanMobil = new TampilanPesanMobil(this);
    this.add(tampilanPesanMobil, ContohGame.MENU_PESAN_MOBIL);
    
    JPanel tampilanPesanMakanan = new TampilanPesanMakanan(this);
    this.add(tampilanPesanMakanan, ContohGame.MENU_PESAN_MAKANAN);
    
    /**
     * Ketika pertama kali game dijalankan, menu yang ditampilkan adalah dashboard
     */
    this.pindahMenu(this.MENU_DASHBOARD);

  }

  /**
   * pindahMenu merupakan fungsi buatan sendiri (jelas bahasanya Indonesia)
   *  untuk memudahkan pindah menu melalui CardLayout berdasarkan konstanta menu yang sudah dibuat
   */
  void pindahMenu(String namaMenu) {

    /**
     * Nama method: show(p1, p2) 
     * Kepunyaan class: CardLayout 
     * Tanggung jawab: menampilkan Card dengan ID tertentu 
     * Parameter:
     *  1. Object JPanel tempat CardLayout diimplementasikan 
     *  2. ID dari Card 
     *
     * Script di bawah ini: Tampilkan menu namaMenu pada JPanel nya JFrame ContohGame ini 
     */
    this.pengelolaMenu.show(this.getContentPane(), namaMenu);

    /**
     * Nama method: setTitle(p1) 
     * Kepunyaan class: JFrame 
     * Tanggung jawab: mengubah judul jendela
     * Parameter:
     *  1. Judul jendela 
     *
     * Script di bawah ini: Ubah judul jendela dari ContohGame dengan isi dari variable namaMenu 
     */
    this.setTitle(namaMenu);
  }

  public static void main(String[] argumen) {

    /**
     * Instansiasi Object dari Class ContohGame yang diwadahi oleh Variable contohGame
     *
     * Misal:
     *  Manusia rahmat = new Manusia();
     *  Kucing siHitamLoreng = new Kucing();
     *
     */
    ContohGame contohGame = new ContohGame();
  }

}
