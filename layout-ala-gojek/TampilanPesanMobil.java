import javax.swing.JPanel; 
import javax.swing.JLabel; 
import javax.swing.JButton; 
import javax.swing.BoxLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

class TampilanPesanMobil extends JPanel {
  TampilanPesanMobil(ContohGame gameIni) {
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    this.add(new JLabel("Pesan mobil ah mau jalan-jalan"));

    JButton tombolDashboard = new JButton("Dashboard") {{
      addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          gameIni.pindahMenu(ContohGame.MENU_DASHBOARD);
        }
      });
    }};

    this.add(tombolDashboard);
  }
}
