import javax.swing.JFrame; 
import javax.swing.JPanel; 
import javax.swing.BoxLayout;
import java.awt.Font;

import javax.swing.JLabel; 
import javax.swing.JButton; 

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ContohGame {
  JFrame jendela;
  JPanel wadahMenuAtas, wadahIntro, wadahDunia;

  ContohGame () {
    System.out.println("Mulai game");
    this.jendela = new JFrame();
    this.jendela.setLayout(new BoxLayout(jendela.getContentPane(), BoxLayout.Y_AXIS));
    this.jendela.setSize(800, 600);
    this.jendela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.jendela.setVisible(true);

    this.inisiasiMenuAtas();
    this.inisiasiWadahIntro();
    this.inisiasiWadahDunia();

    this.sembunyikanSemuaWadah();
    this.jendela.setTitle("Kisah Hachi si Lebah Madu");
    this.wadahIntro.setVisible(true);
  }

  void inisiasiMenuAtas () {
    this.wadahMenuAtas = new JPanel();
    this.wadahMenuAtas.setVisible(true);
    this.wadahMenuAtas.setLayout(new BoxLayout(this.wadahMenuAtas, BoxLayout.X_AXIS));

    ContohGame gameIni = this;

    JButton tombolIntro = new JButton("Intro");
    tombolIntro.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        gameIni.sembunyikanSemuaWadah();
        gameIni.jendela.setTitle("Kisah Hachi si Lebah Madu");
        gameIni.wadahIntro.setVisible(true);
      }
    });
    this.wadahMenuAtas.add(tombolIntro);

    JButton tombolDunia = new JButton("Dunia");
    tombolDunia.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        gameIni.sembunyikanSemuaWadah();
        gameIni.jendela.setTitle("Peta Dunia Hachi");
        gameIni.wadahDunia.setVisible(true);
      }
    });
    this.wadahMenuAtas.add(tombolDunia);

    this.jendela.add(this.wadahMenuAtas);
  }

  void inisiasiWadahIntro () {
    this.wadahIntro = new JPanel();
    this.wadahIntro.setVisible(false);
    this.wadahIntro.setLayout(new BoxLayout(this.wadahIntro, BoxLayout.Y_AXIS));

    JLabel tulisanIntro = new JLabel("بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِيْمِ");
    tulisanIntro.setFont(new Font("Verdana", Font.PLAIN, 36));

    JLabel tulisanTerjemah = new JLabel("Dengan menyebut nama Allah yang Maha Pengasih lagi Maha Penyayang");
    tulisanTerjemah.setFont(new Font("Verdana", Font.PLAIN, 18));
    this.wadahIntro.add(tulisanIntro);
    this.wadahIntro.add(tulisanTerjemah);

    ContohGame gameIni = this;

    this.jendela.add(this.wadahIntro);
  }

  void inisiasiWadahDunia () {
    this.wadahDunia = new JPanel();
    this.wadahDunia.setVisible(false);
    this.wadahDunia.setLayout(new BoxLayout(this.wadahDunia, BoxLayout.Y_AXIS));

    JLabel tulisan = new JLabel("Peta dunia Hachi");
    this.wadahDunia.add(tulisan);

    this.jendela.add(this.wadahDunia);
  }

  void sembunyikanSemuaWadah () {
    this.wadahIntro.setVisible(false);
    this.wadahDunia.setVisible(false);
  }

  public static void main (String[] argumen) {
    ContohGame contohGame = new ContohGame();
  }
}
